uvicorn \
  --uds ./uvicorn.sock \
  --ssl-keyfile ./config/nginx/certs/where2stay.ssl.key \
  --ssl-certfile ./config/nginx/certs/where2stay.ssl.crt \
  --lifespan off \
  --log-level debug \
  --reload \
  config.asgi:application

# uvicorn --uds ./uvicorn.sock config.asgi:application
  # --reload \

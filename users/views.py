from django.views.generic.base import TemplateView
from rest_framework.viewsets import ModelViewSet

from .serializers import UserSerializer
from .models import User


# ViewSets define the view behavior.
class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileView(TemplateView):
    template_name = "account/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['items'] = [
            'Guest: list of bookings (current and past)',
            'Host: list of rooms',
            'Other useful information?',
        ]

        return context

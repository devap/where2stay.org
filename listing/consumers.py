from channels.generic.websocket import AsyncJsonWebsocketConsumer
from rest_framework.renderers import JSONRenderer

from .models import Listing
from .serializers import ListingSerializer


class JSONConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        # print(dir(self.scope["session"]))
        session_key = self.scope['session']['session_key']
        print('ws.connect()->session_key: ', session_key)
        print('ws.connect()->channel_layer: ', self.channel_layer)
        print('ws.connect()->channel_name: ', self.channel_name)

        await self.channel_layer.group_add(session_key, self.channel_name)

        self.user = self.scope["user"]
        if self.user.is_authenticated:
            print('user ', self.user)
        else:
            print('Unauthenticated')

        await self.accept()
        # call now or on receive_json?
        # await self.msg_listings()
        print('leaving connect()..')

    async def disconnect(self, close_code):
        session_key = self.scope['session']['session_key']
        await self.channel_layer.group_discard(session_key, self.channel_name)

    async def receive_json(self, event):
        print('ws.receive_json()->event: ', event)
        await self.send_listings({
            "type": event["type"],
            "destination": event["destination"],
        })

    async def send_listings(self, data):

        from datetime import datetime
        timestamp = datetime.now().time()
        """
        Called when someone has listings for our client.
        """
        listings = Listing.active.all()
        # listings = Listing.active.filter(city__name=data["destination"])
        serializer = ListingSerializer(listings, many=True)
        listings = JSONRenderer().render(serializer.data)
        listings = listings.decode('utf-8')
        print('JSONConsumer.send_listings(%s)' % timestamp)
        print('JSONConsumer.send_listings()->listings: ', listings)
        # print('serializer.data: ', serializer.data)

        await self.send_json(
            {
                "listings": listings,
            },
        )

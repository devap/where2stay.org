import React from 'react';
import PropTypes from 'prop-types';
// import DatePickerField from './date-picker';
import CSRFToken from './csrftoken';

import DatePickerField from './date-picker';
import Adults from './Adults';
import Children from './Children';
import Rooms from './Rooms';

/*
const DatePickerField = lazy(() => import('./date-picker'));
const Adults = lazy(() => import('./Adults'));
const Children = lazy(() => import('./Children'));
const Rooms = lazy(() => import('./Rooms'));
*/

const _ = require('lodash');


const QueryForm = (props) => {
  const {
    values,
    touched,
    errors,
    setSubmitting,
    setFieldValue,
  } = props;

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('values: ', values);

    if (_.isEmpty(errors)) {
      const frm = document.getElementById('queryForm');
      frm.submit();
    } else {
      const hasErrors = (_.isEmpty(errors)) ? 'No' : 'Yes';
      console.log('hasErrors: ', hasErrors);
      console.log('errors: ', errors);
    }

    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 10000);
  };

  const handleChange = (e) => {
    const myName = e.target.name;
    setFieldValue(myName, e.target.value);
  };

  return (
    <form
      id="queryForm"
      method="POST"
      action="/listings/"
      onSubmit={handleSubmit}
    >
      <CSRFToken />
      <input
        type="hidden"
        name="startDateStr"
        value={values.startDateStr}
      />
      <input
        type="hidden"
        name="endDateStr"
        value={values.endDateStr}
      />

      <input
        type="text"
        name="destination"
        className="query-panel destination"
        placeholder="Destination City"
        onChange={handleChange}
        value={values.destination}
        required
      />
      {errors.destination && touched.destination && <div id="feedback" className="error-message alert alert-primary">{errors.destination}</div>}

      <DatePickerField
        values={values}
        setFieldValue={setFieldValue}
      />
      {(errors.startDate || errors.endDate) && <div id="feedback" className="error-message alert alert-primary">Please select Start and End Dates</div>}

      <Adults
        value={values.bookAdults}
        changeHandler={handleChange}
      />

      <Children
        value={values.bookChildren}
        changeHandler={handleChange}
      />

      <Rooms
        value={values.bookRooms}
        changeHandler={handleChange}
      />

      <button type="submit">
        Search
      </button>

    </form>
  );
};

QueryForm.propTypes = {
  values: PropTypes.shape({
    destination: PropTypes.string.isRequired,
    bookAdults: PropTypes.string.isRequired,
    bookChildren: PropTypes.string.isRequired,
    bookRooms: PropTypes.string.isRequired,
    startDate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
    endDate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
    startDateStr: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
    endDateStr: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
  }).isRequired,
  touched: PropTypes.shape({
    destination: PropTypes.bool,
    startDate: PropTypes.bool,
    endDate: PropTypes.bool,
    bookAdults: PropTypes.bool,
    bookChildren: PropTypes.bool,
    bookRooms: PropTypes.bool,
  }).isRequired,
  errors: PropTypes.shape({
    destination: PropTypes.string,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    bookAdults: PropTypes.string,
    bookChildren: PropTypes.string,
    bookRooms: PropTypes.string,
  }).isRequired,
  setSubmitting: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
};

QueryForm.defaultProps = {

};

export default QueryForm;

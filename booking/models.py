from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Booking(models.Model):

    name = models.CharField(
        _('name of booking'),
        max_length=155,
     )

    host = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='bookings',
        verbose_name=_('host'),
      )

    class Meta:
        ordering = ("-name",)
        get_latest_by = ("name")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('booking', kwargs={'booking_id': self.id})

import React from 'react';
import PropTypes from 'prop-types';

/*
const adultsCnt = 51;
const options = [];
for (let i = 1; i < adultsCnt; i += 1) {
  const option = {
    value: i,
    label: (i > 1) ? `${i} Adults` : `${i} Adult`,
  };
  options.push(option);
}
*/

const Adults = ({
  value,
  changeHandler,
}) => (
  <select
    name="bookAdults"
    value={value}
    onChange={changeHandler}
  >
    <option value="1">1 Adult</option>
    <option value="2">2 Adults</option>
    <option value="3">3 Adults</option>
    <option value="4">4 Adults</option>
    <option value="5">5 Adults</option>
    <option value="6">6 or more Adults</option>
  </select>
);

Adults.propTypes = {
  value: PropTypes.string.isRequired,
  changeHandler: PropTypes.func.isRequired,
};

export default Adults;

from django.urls import path
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from channels.sessions import SessionMiddlewareStack

# from . import consumers
from .consumers import JSONConsumer


websocket_urlpatterns = [
    path('ws/', JSONConsumer),
    # path('ws/<int:listing_id>/', ListingConsumer),
]


application = ProtocolTypeRouter({
    "websocket": AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            SessionMiddlewareStack(
                URLRouter(
                    websocket_urlpatterns
                )
            ),
        ),
    ),
})

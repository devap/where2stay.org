import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible

from crispy_forms.bootstrap import InlineField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit


class BookingLookup(forms.Form):

    destination = forms.CharField(max_length=100)
    date_from = forms.DateField()
    date_to = forms.DateInput()
    num_adult = forms.IntegerField()
    num_child = forms.IntegerField()
    num_rooms = forms.IntegerField()
    captcha = ReCaptchaField(widget=ReCaptchaV2Invisible)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.field_template = 'forms/inline_field.html'
        self.helper.form_class = 'form-inline'
        self.helper.form_id = 'list-bookings'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.label_class = 'col-4 text-right col-form-label'
        self.helper.field_class = 'col-8'
        self.helper.help_text_inline = True

        self.helper.layout = Layout(
            'destination',
            'date_from',
            'date_to',
            'num_adult',
            'num_child',
            'num_rooms'
        )

        '''
        self.helper.layout = Layout(
            InlineField('destination'),
            InlineField('date_from'),
            InlineField('date_to'),
            InlineField('num_adult'),
            InlineField('num_child'),
            InlineField('num_rooms')
        )
        '''

        self.helper.add_input(Submit('submit', 'Submit'))

    def clean_travel_date_to(self):
        date_from = self.cleaned_data['travel_date_from']
        date_to = self.cleaned_data['travel_date_to']

        # Check if a date is not in the past.
        if date_to < date_from:
            raise ValidationError(_('Invalid date - From date must be after To date'))

        return date_to

    def clean_travel_date_from(self):
        date_from = self.cleaned_data['travel_date_from']

        # Check if a date is in the allowed range (+4 weeks from today).
        if date_from < datetime.date.today():
            raise ValidationError(_('Invalid date - From date cannot be in the past'))

        return date_from


class FeedbackForm(forms.Form):
    """ Crispy Version """
    sender = forms.EmailField(label='Sender Email', required=True)
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    captcha = ReCaptchaField(widget=ReCaptchaV2Invisible)

    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'feedback'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.label_class = 'col-4 text-right col-form-label'
        self.helper.field_class = 'col-8'
        self.helper.help_text_inline = True

        self.helper.layout = Layout(
            InlineField('sender'),
            InlineField('subject'),
            InlineField('message'),
        )

        self.helper.add_input(Submit('submit', 'Submit'))


class QuickSearchForm(forms.Form):
    gender = forms.CharField(max_length=100)

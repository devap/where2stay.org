import React from 'react';

import { StateProvider } from './ContextState';

import Filters from './Filters';
import ListingsWrapper from './ListingWrapper';
import LocationMap from './LocationMap';


const App = (props) => {
  const { query } = props;
  console.log('query: ', query);
  const initialState = {
    query,
    listings: [],
    filteredListings: [],
    filters: [],
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'updateListings':
        return {
          ...state,
          listings: action.listings
        };

      case 'updateFilteredListings':
        return {
          ...state,
          filteredListings: action.filteredListings
        };

      case 'updateFilters':
        return {
          ...state,
          filters: action.newFilters
        };

      default:
        return state;
    }
  };

  return (
    <StateProvider
      initialState={initialState}
      reducer={reducer}
    >
      <Filters />
      <ListingsWrapper />
      <LocationMap />
    </StateProvider>
  );
};

export default App;

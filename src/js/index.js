import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App_index';

require('../styl/homepage.styl');

const rootNode = document.getElementById('app_node');

ReactDOM.render(
  <App />,
  rootNode,
);

console.log('index.js loaded');

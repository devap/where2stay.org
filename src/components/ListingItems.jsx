import React from 'react';
import Amenities from './ListingItemsAmenities';

const Item = props => (
  <li>
    <div className="field">{props.field}</div>
    <div className="value">{props.value}</div>
  </li>
);

const ListingItems = (props) => {
  const {
    amenities,
    city,
    cost,
    country,
    state_province,
    number_of_rooms,
    distance_from_temple,
    distance_from_temple_units,
    distance_from_temple_restaurant,
    distance_from_temple_restaurant_units,
    distance_from_vegetarian_restaurant,
    distance_from_vegetarian_restaurant_units,
  } = props.fields;

  const attribs = {
    distance_from_temple,
    distance_from_temple_units,
    distance_from_temple_restaurant,
    distance_from_temple_restaurant_units,
    distance_from_vegetarian_restaurant,
    distance_from_vegetarian_restaurant_units,
  };

  const getCost = () => (
    `${country.currency_symbol} ${cost} ${country.currency}`
  );

  return (
    <ul>
      <Item field="Country:" value={country.name} />
      <Item field="State/Province:" value={state_province.name} />
      <Item field="City:" value={city.name} />
      <Item field="# of Rooms Available:" value={number_of_rooms} />
      <Item field="Cost:" value={getCost()} />

      <li className="amenities">Amenities:</li>

      <Amenities attribs={attribs} amenities={amenities} />
    </ul>
  );
};

export default ListingItems;

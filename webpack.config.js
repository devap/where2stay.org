const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {

    context: __dirname, // to automatically find tsconfig.json

    devServer: {
        contentBase: path.join(__dirname, '.'),
        compress: true,
        port: 8080
    },

    devtool: 'inline-source-map',

    entry: {
        vendor: './src/js/vendor.js',
        nanogallery2: './src/js/nanogallery2.js',
        where2stay: './src/js/where2stay.js',
        homepage: './src/js/index.js',
        listings: './src/js/listings.js',
        bookings: './src/js/bookings.js',
        admin: './src/js/admin.js',
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                include: [
                    path.resolve(__dirname, 'src'),
                ],
                exclude: /node_modules/,
                use: [
                {
                    loader: 'thread-loader',
                    options: {
                        workers: 2,
                        workerParallelJobs: 50,
                        workerNodeArgs: ['--max-old-space-size=1024'],
                        poolTimeout: 2000,
                    }
                },
                    'babel-loader',
                    'eslint-loader'
                ],
            },
            {
                test: /\.(css|sass|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.styl$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'stylus-loader'
                ]
            },
            {
                test: require.resolve('shifty'),
                use: [
                    { loader: 'expose-loader', options: 'NGTweenable' },
                ]
            },
            {
                test: require.resolve('hammerjs'),
                use: [
                    { loader: 'expose-loader', options: 'NGHammer' },
                ]
            },
            {
                test: require.resolve('imagesloaded'),
                use: [
                    { loader: 'expose-loader', options: 'ngimagesLoaded' },
                    { loader: 'expose-loader', options: 'ngImagesLoaded' },
                ]
            },
            {
                test: require.resolve('screenfull'),
                use: [
                    { loader: 'expose-loader', options: 'ngscreenfull' },
                ]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: 'url-loader?limit=10000&mimetype=application/font-woff' }
                ]
            },
            {
                test: /\.(ttf|eot|otf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: ['file-loader']
            },
            {
                test: /\.(jpe?g|gif|png|mp3|svg)$/i,
                use: ['file-loader']
            },
        ]
    },

    output: {
        //filename: '[name].[chunkhash].js',
        filename: '[name].js',
        path: path.resolve(__dirname, './static/dist')
    },

    optimization: {
        minimizer: [
              new UglifyJsPlugin({
                chunkFilter: (chunk) => {
                  // Exclude uglification for the `vendor` chunk
                  if (chunk.name === 'vendor') {
                    return false;
                  }

                  return true;
                },
              }),
            ],
        splitChunks: {
            cacheGroups: {
                vendors: {
                    priority: -10,
                    test: /[\\/]node_modules[\\/]/
                },
            },

            chunks: 'async',
            minChunks: 1,
            minSize: 30000,
            name: true
        }
    },

    plugins: [
        new webpack.ProvidePlugin({
           $: 'jquery',
           jQuery: 'jquery',
           _: 'lodash',
           Bootstrap: 'bootstrap',
           React: 'react',
           Tether: 'tether',
           tether: 'tether',
          }),
        new MiniCssExtractPlugin({
              filename: '[name].css',
              chunkFilename: '[id].css',
              ignoreOrder: false, // Enable to remove warnings about conflicting order
            }),
        new FriendlyErrorsWebpackPlugin(),
        /**
        new CopyPlugin([
            {
                from: './src/iconic/js',
                to: 'iconic/js',
                toType: 'dir',
            },
            {
                from: './src/iconic/svgz/smart',
                to: 'iconic/svgz',
                toType: 'dir',
            },
            {
                from: './src/iconic/svg/smart',
                to: 'iconic/svg',
                toType: 'dir',
            },
        ]),
        */
    ],

    resolve: {
        /*
        alias: {
          'react-dom$': 'react-dom/profiling',
          'scheduler/tracing': 'scheduler/tracing-profiling',
        },
        */
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        symlinks: false,
    },

    watchOptions: {
        ignored: /node_modules/
    },
};

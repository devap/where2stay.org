# create user/role
#createuser -d -P --interactive where2stay

#create database
# createdb -O where2stay -T template0 where2stay

psql -c "CREATE ROLE where2stay WITH LOGIN PASSWORD 'where2stay'";
psql -c "CREATE DATABASE where2stay OWNER=where2stay TEMPLATE=template0";
psql -c "GRANT ALL PRIVILEGES ON DATABASE where2stay TO where2stay";

# Step 2: Load the PostGIS Extensions
psql -c "CREATE EXTENSION postgis;" where2stay
psql -c "CREATE EXTENSION fuzzystrmatch;" where2stay
psql -c "CREATE EXTENSION postgis_tiger_geocoder;" where2stay
psql -c "CREATE EXTENSION postgis_topology;" where2stay

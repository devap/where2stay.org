from os.path import join
from configparser import RawConfigParser

from .__base import *
from ._paths import *
from ._apps import INSTALLED_APPS
from ._cache import (
  ADV_CACHE_RESOLVE_NAME,
  SESSION_ENGINE,
  SESSION_CACHE_ALIAS,
  CACHE_COUNT_TIMEOUT,
  CACHE_TIMEOUT,
  CACHES,
)
from ._filebrowser import *
from ._logging import LOGGING
from ._middleware import (
  MIDDLEWARE,
  SERIALIZATION_MODULES,
  THUMBNAIL_FORMAT,
  STATICFILES_FINDERS,
  STATICFILES_STORAGE,
)
from ._rest import REST_FRAMEWORK
from ._search import (
  HAYSTACK_SEARCH_RESULTS_PER_PAGE,
  HAYSTACK_SIGNAL_PROCESSOR,
  HAYSTACK_CONNECTIONS,
)
from ._templates import TEMPLATES

"""
from ._tinymce import (
  TINYMCE_JS_URL,
  TINYMCE_JS_ROOT,
  TINYMCE_DEFAULT_CONFIG,
  TINYMCE_DISABLE_CLEANING,
  TINYMCE_ALLOWED_TAGS,
  TINYMCE_ALLOWED_ATTRIBUTES,
  TINYMCE_ALLOWED_STYLES,
)
"""


config = RawConfigParser()
config_file = './config/uwsgi.ini'
config.read(config_file)

DEBUG = True
DEBUG_TOOLBAR = DEBUG
FILEBROWSER_DEBUG = False
TASTYPIE_FULL_DEBUG = DEBUG
CRISPY_FAIL_SILENTLY = not DEBUG

MEDIA_ROOT = '/Users/devaprastha/data/where2stay/media/'
STATIC_ROOT = '/Users/devaprastha/data/where2stay/static/'

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

if DEBUG_TOOLBAR:

    from ._debug_toolbar import (
        DEBUG_TOOLBAR_PATCH_SETTINGS,
        DEBUG_TOOLBAR_CONFIG,
        DEBUG_TOOLBAR_PANELS,
        MIDDLEWARE,
    )

    INSTALLED_APPS += (
        'debug_toolbar',
    )

    def show_toolbar(request):
        if DEBUG:
          return True

    DEBUG_TOOLBAR_CONFIG['SHOW_TOOLBAR_CALLBACK'] = show_toolbar


# SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']
RECAPTCHA_PRIVATE_KEY = config.get('django', 'GOOGLE_RECAPTCHA_SECRET_KEY')

PROJET_DOMAIN_NAME = 'where2stay.local'

### TinyMCE
TINYMCE_JS_URL = join(STATIC_URL, 'tiny_mce/tiny_mce.js')
TINYMCE_JS_ROOT = join(STATIC_ROOT, 'tiny_mce/')

### Filebrowser
ADMIN_MEDIA_PREFIX = STATIC_URL + 'grappelli/'
FILEBROWSER_MEDIA_ROOT = MEDIA_ROOT
FILEBROWSER_MEDIA_URL = MEDIA_URL
FILEBROWSER_PATH_FILEBROWSER_MEDIA = join(STATIC_ROOT, 'filebrowser/')
FILEBROWSER_URL_FILEBROWSER_MEDIA = join(STATIC_URL, 'filebrowser/')
FILEBROWSER_URL_TINYMCE = join(STATIC_URL, 'grappelli/tinymce/jscripts/tiny_mce/')
FILEBROWSER_PATH_TINYMCE = join(STATIC_ROOT, 'grappelli/tinymce/jscripts/tiny_mce/')
### end Filebrowser

GRAPPELLI_ADMIN_TITLE = 'where2stay'
GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
#GRAPPELLI_ADMIN_URL =

SECRET_KEY = config.get('django', 'SECRET_KEY')
CSRF_COOKIE_DOMAIN = SESSION_COOKIE_DOMAIN = None

INSTALLED_APPS += (
    #'memcache_toolbar',
    #'haystack_panel',
)

ALLOWED_HOSTS = (
  PROJET_DOMAIN_NAME,
  '127.0.0.1',
  'localhost',
)

INTERNAL_IPS = (
  '127.0.0.1',
)

SECURE_HSTS_INCLUDE_SUBDOMAINS = True

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

########## DATABASE CONFIGURATION
DATABASES['default']['NAME'] = config.get('database', 'DATABASE_NAME')
DATABASES['default']['USER'] = config.get('database', 'DATABASE_USER')
DATABASES['default']['PASSWORD'] = config.get('database', 'DATABASE_PASSWORD')
########## END DATABASE CONFIGURATION

########## CACHE CONFIGURATION
#CACHES['default']['BACKEND'] = 'django.core.cache.backends.dummy.DummyCache'
CACHES['default']['LOCATION'] = config.get('cache', 'REDIS_URL')
CACHES['default']['KEY_PREFIX'] = config.get('cache', 'KEY_PREFIX')
########## END CACHE CONFIGURATION

########## SEARCH CONFIGURATION
HAYSTACK_CONNECTIONS['default']['INDEX_NAME'] = config.get('search', 'INDEX_NAME')
HAYSTACK_CONNECTIONS['default']['URL'] = config.get('search', 'URL')
########## END SEARCH CONFIGURATION

MIDDLEWARE += (
  #'services.middleware.ResponseLoggingMiddleware',
)

import React from 'react';
import _ from 'lodash';
import { getState } from './ContextState';


const filterItems = [
  { id: 1, label: 'Cost', value: 'cost' },
  { id: 2, label: 'Shared Room', value: 'shared_room' },
  { id: 3, label: 'Private Room', value: 'private_room' },
  { id: 4, label: 'Shared Bathroom', value: 'shared_bathroom' },
  { id: 5, label: 'Private Bathroom', value: 'private_bathroom' },
  { id: 6, label: 'Whole Apartment', value: 'whole_apartment' },
  { id: 7, label: 'Prasadam Included', value: 'prasadam_included' },
  // { id: 9, label: 'Proximity to ISKCON Temple', value: 'cost' },
  // { id: 10, label: 'Proximity to Hare Krishna Restaurant', value: 'cost' },
  { id: 11, label: 'Care for Hire Available', value: 'car_for_hire_available' },
  { id: 12, label: 'Airport Pickup Available', value: 'airport_pickup_available' },
];

const Filters = () => {
  const [{ listings }] = getState();
  const [{ filteredListings }, setFilteredListings] = getState();
  // let [{ filteredListings }] = getState();
  // const [, setFilteredListings] = getState();
  const [{ filters }, setFilters] = getState();

  const changeHandler = (event) => {
    const attrib = event.target.name;
    if (event.target.checked && !filters.includes(attrib)) {
      filters.push(attrib);
    } else if (!event.target.checked && filters.includes(attrib)) {
      _.pull(filters, attrib);
    }

    setFilters({
      type: 'updateFilters',
      newFilters: filters,
    });


    const filtered = listings.filter((item) => {
      let hasKey = false;
      Object.keys(item.amenities).forEach((key) => {
        if (item.amenities[key] === true && filters.includes(key)) {
          hasKey = true;
        }
      });
      return hasKey;
    });

    setFilteredListings({
      type: 'updateFilteredListings',
      filteredListings: filters.length ? filtered : listings,
    });
  };

  const FiltersArry = filterItems.map(item => (
    <li className="filters-checkbox" key={item.id}>
      <input type="checkbox" name={item.value} onChange={changeHandler} />
      {item.label}
    </li>
  ));

  return (
    <div className="col-2">
      <h2 className="filters_heading">Filters ...</h2>
      <ul>
        {FiltersArry}
      </ul>
    </div>
  );
};

export default Filters;

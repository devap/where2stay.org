from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from allauth.account.forms import SignupForm

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible

from .models import User


class CustomSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('email',
                  'first_name',
                  'last_name',
                  'is_host',
                  'is_guest',
                  )
        captcha = ReCaptchaField(widget=ReCaptchaV2Invisible)


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('email',
                  'first_name',
                  'last_name',
                  'is_host',
                  'is_guest',
                  'is_active',
                  'is_superuser'
                  )
        captcha = ReCaptchaField(widget=ReCaptchaV2Invisible)

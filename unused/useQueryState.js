import { useStateContext } from './State';

const useQueryState = () => {
  const [stateContext, dispatch] = useStateContext();

  function submitHandler(event) {
    console.log('stateContext: ', stateContext);
    console.log('event.target: ', event.target);
    console.log('event.target.destination.value: ', event.target.destination.value);
    console.log('stateContext.startDate: ', stateContext.startDate);
    console.log('stateContext.endDate: ', stateContext.endDate);
    if (event.target.destination.value === ''
      || stateContext.startDate === null
      || stateContext.endDate === null
    ) {
      // return true;
      console.log('returning false');
      event.preventDefault();
    }
  }

  return {
    stateContext,
    dispatch,
    submitHandler,
  };
};

export default useQueryState;

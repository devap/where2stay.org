################
# APPLICATIONS #
################
DJANGO_APPS = (
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.humanize',
  'django.contrib.sessions',
  'django.contrib.sites',
  'django.contrib.messages',
  'django.contrib.staticfiles',
  'django.contrib.redirects',
  'django.contrib.gis',

)

DJANGO_PRE_ADMIN = (
  # 'grappelli.dashboard',
  # 'grappelli',
  # 'filebrowser',

)

DJANGO_ADMIN = (
  # Uncomment the next line to enable the admin:
  'django.contrib.admin',

)

THIRD_PARTY_APPS = (
  'allauth',
  'allauth.account',
  'allauth.socialaccount',
  # 'allauth.socialaccount.providers.amazon',
  # 'allauth.socialaccount.providers.facebook',
  'allauth.socialaccount.providers.google',

  'captcha',
  'channels',
  'cities',
  'crispy_forms',
  'django_extensions',
  'djreservation',
  'rest_framework',
  'rest_framework.authtoken',
  'smart_selects',
  'tagulous',

)

# Apps specific for this project go here.
LOCAL_APPS = (
  'booking',
  'content',
  'listing',
  'users',
  'utils',
)

INSTALLED_APPS = DJANGO_APPS + DJANGO_PRE_ADMIN + DJANGO_ADMIN + THIRD_PARTY_APPS + LOCAL_APPS
########## END APP CONFIGURATION

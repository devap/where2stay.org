/** * nanogallery2 js ** */

import 'shifty';
import 'hammerjs';
import 'imagesloaded';
import 'screenfull';
import 'nanogallery2/src/jquery.nanogallery2.core';
import 'nanogallery2/src/css/nanogallery2.css';

global.jQuery = require('jquery');
global.$ = require('jquery');

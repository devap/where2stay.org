from django.core.management.base import BaseCommand, CommandError

import pycountry

from utils import getCurrency
from utils.models import Country, Region


class Command(BaseCommand):

    help = 'imports data from pycountry into database'

    def handle(self, *args, **options):

        for country in pycountry.countries:
            name = country.name
            code2 = country.alpha_2
            code3 = country.alpha_3
            numeric = country.numeric
            (currency_code, currency_name) = getCurrency(numeric)
            print('country: ', name, code2, code3, numeric, currency_code, currency_name)
            c = Country(name=name)
            c.code2 = code2
            c.code3 = code3
            c.numeric = numeric
            if currency_code:
                c.currency_code = currency_code
            if currency_name:
                c.currency_name = currency_name
            c.save()

            regions = pycountry.subdivisions.get(country_code=code2)
            for region in regions:
                print('\tregion: ', region.name, region.code, numeric)
                r = Region(
                    name=region.name,
                    code=region.code,
                    country=numeric
                 )
                r.save()

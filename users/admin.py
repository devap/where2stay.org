from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.decorators import login_required

from .models import User
from .forms import SignupForm, CustomUserCreationForm, CustomUserChangeForm


admin.site.login = login_required(admin.site.login)


@admin.register(User)
class UserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ('email',
                    'is_staff',
                    'is_active',
                    'is_host',
                    'is_guest',
                    )
    list_filter = ('email',
                   'is_staff',
                   'is_active',
                   'is_host',
                   'is_guest',
                   )
    fieldsets = (
        (None, {'fields': ('email',
                           'password',
                           'first_name',
                           'last_name',
                           'is_host',
                           'is_guest',
                           )
                }
         ),
        ('Permissions', {'fields': ('is_admin',
                                    'is_superuser',
                                    'is_staff',
                                    'is_active',
                                    'groups',
                                    )
                         }
         ),
    )
    add_fieldsets = (
        (None, {'classes': ('wide',),
                'fields': ('email',
                           'first_name',
                           'last_name',
                           'password1',
                           'password2',
                           'is_staff',
                           'is_superuser',
                           'is_active',
                           'is_host',
                           'is_guest',
                           )
                }
         )
    )
    search_fields = ('email',)
    ordering = ('email',)

import { withFormik } from 'formik';

import QueryForm from './QueryForm';
import Validations from './yupValidations';

/* Initial Values */
const formInitialValues = {
  destination: '',
  startDate: '',
  endDate: '',
  startDateStr: '',
  endDateStr: '',
  bookAdults: '1',
  bookChildren: '0',
  bookRooms: '1',
};

const App = withFormik({
  // enableReinitialize: true,
  mapPropsToValues: () => (formInitialValues),
  validationSchema: Validations,
  validateOnBlur: true,
  validateOnChange: false,
})(QueryForm);

export default App;

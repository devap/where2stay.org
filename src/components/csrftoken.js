import React from 'react';
import Cookies from 'js-cookie/src/js.cookie';

const csrftoken = Cookies.get('csrftoken');
const CSRFToken = () => (
  <input
    type="hidden"
    name="csrfmiddlewaretoken"
    value={csrftoken}
  />
);

export default CSRFToken;

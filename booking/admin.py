from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .models import Booking

admin.site.login = login_required(admin.site.login)

'''
class BookingAdminSite(admin.AdminSite):
    booking_site_header = 'Where2Stay Booking Admin'
    site_title = 'Where2Stay Booking Admin Portal'
    index_title = 'Welcome to Where2Stay Booking Admin Portal'


# admin.site.login = login_required(admin.site.login)
booking_admin_site = BookingAdminSite(name='booking_admin')
booking_admin_site.login = login_required(admin.site.login)

booking_admin_site.register(Booking)
'''

'''
@booking_admin_site.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    pass
'''

admin.site.register(Booking)

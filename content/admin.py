from django.contrib import admin
from django.contrib.auth.decorators import login_required
# import tagulous

from .models import Page


admin.site.login = login_required(admin.site.login)


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
  """
  Admin class for pages
  """

  ordering = ['-id', ]
  list_display = ['id',
                  'title',
                  'status',
                  'created',
                  'modified',
                  'publish_date'
                  ]

  list_filter = ['status', 'publish_date', ]
  list_display_links = ['id', 'title', ]
  readonly_fields = []
  date_hierarchy = 'publish_date'

  search_fields = ['id',
                   'title',
                   'content'
                   ]

  fieldsets = (
    ("Metadata",
      {"fields":
        ("status", )
      }
    ),
    ("Content",
      {"fields":
        ("title", "content", )
      }
    ),
    ("Info",
      {"fields":
        ("slug", "publish_date", ),
       "classes": ('grp-collapse grp-closed',)
      }
    ),
  )

  class Media:
    js = [
      '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
      '/static/grappelli/tinymce_setup/tinymce_setup.js',
    ]

  def get_tags(self, obj):
    tags = ', '.join([s.name for s in obj.tags.all()])
    return tags
  get_tags.short_description = "Tags"

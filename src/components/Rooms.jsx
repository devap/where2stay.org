import React from 'react';
import PropTypes from 'prop-types';

/*
const roomCnt = 51;
const options = [];
for (let i = 0; i < roomCnt; i += 1) {
  const option = {
    value: i,
    label: (i > 1) ? `${i} Rooms` : `${i} Room`,
  };
  options.push(option);
}
*/

const Rooms = ({
  value,
  changeHandler,
}) => (
  <select
    name="bookRooms"
    value={value}
    onChange={changeHandler}
  >
    <option value="1">1 Rooms</option>
    <option value="2">2 Rooms</option>
    <option value="3">3 Rooms</option>
    <option value="4">4 Rooms</option>
    <option value="5">5 Rooms</option>
    <option value="6">6 or more Rooms</option>
  </select>
);

Rooms.propTypes = {
  value: PropTypes.string.isRequired,
  changeHandler: PropTypes.func.isRequired,
};


export default Rooms;

import React, { useState } from 'react';
// import React from 'react';
import PropTypes from 'prop-types';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';

const momentPropTypes = require('react-moment-proptypes');

// from https://codesandbox.io/s/723l233my1?from-embed
const DatePickerWithFormik = ({
  form: {
    values,
    setFieldValue,
  },
  field,
  startDate,
  endDate,
  ...props,
}) => {
  console.log('DatePickerWithFormik->values: ', values);
  console.log('DatePickerWithFormik->field: ', field);
  console.log('DatePickerWithFormik->startDate: ', startDate);
  console.log('DatePickerWithFormik->endDate: ', endDate);
  console.log('DatePickerWithFormik->props: ', props);

  const [focusedInput, setFocusedInput] = useState(null);

  return (
    <DateRangePicker
      startDate={startDate}
      startDateId="Start"
      endDate={endDate}
      endDateId="End"
      onDatesChange={() => {
        setFieldValue('startDate', startDate);
        setFieldValue('endDate', endDate);
      }}
      focusedInput={focusedInput}
      onFocusChange={() => setFocusedInput(focusedInput)}
    />
  );
};

DatePickerWithFormik.propTypes = {
  form: PropTypes.shape({
    setFieldValue: PropTypes.func.isRequired,
    values: PropTypes.shape({
      focusedInput: PropTypes.func,
    }),
  }),
  field: PropTypes.shape({}).isRequired,
  startDate: PropTypes.oneOfType([
    PropTypes.object,
    momentPropTypes.momentObj,
  ]).isRequired,
  endDate: momentPropTypes.momentObj,
};

DatePickerWithFormik.defaultProps = {
  form: {
    values: null,
  },
  endDate: null,
};

export default DatePickerWithFormik;

from django.urls import include, path
# from django.contrib.auth.models import User
from rest_framework import routers

from users.views import UserViewSet
from listing.views import ListingViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'listings', ListingViewSet)

rest_patterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

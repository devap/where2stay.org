import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App_listings';

require('../styl/listings.styl');

const rootNode = document.getElementById('app_node_listings');

ReactDOM.render(
  <App query={window.listings_query} />,
  rootNode,
);

console.log('listings.js loaded');

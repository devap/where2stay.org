# -*- coding: utf-8 -*-
from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from model_utils import Choices
from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel


# Create your models here.
class Page(TimeStampedModel, models.Model):
    """A simple page model. """
    publish_date = models.DateTimeField(
        _('publish date'),
        blank=True,
        null=True,
        default=None
     )

    STATUS = Choices(
        'draft',
        'published'
    )

    status = StatusField(
        db_index=True
    )

    title = models.CharField(
        _('title'),
        max_length=255,
        blank=False
    )

    content = models.TextField(
        _('content'),
        blank=True
    )

    class Meta:
        ordering = ("-publish_date",)
        get_latest_by = ("publish_date")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page', kwargs={'page_id': self.id})

# -*- coding: utf-8 -*-
import pycountry
import moneyed

__version__ = '0.1.0'
__version_info__ = tuple([int(num) if num.isdigit() else num for num in __version__.replace('-', '.', 1).split('.')])

default_app_config = 'utils.apps.UtilsConfig'


def getCurrency(numeric):
    currency_code = None
    currency_name = None
    if (numeric):
        try:
            currency_code = str(moneyed.get_currency(None, numeric))
            currency_name = moneyed.CURRENCIES[currency_code].name
        except moneyed.CurrencyDoesNotExist:
            pass
    return (currency_code, currency_name)


COUNTRIES = {}
for country in pycountry.countries:
    code2 = country.alpha_2
    code3 = country.alpha_3
    numeric = country.numeric
    (currency_code, currency_name) = getCurrency(numeric)
    COUNTRIES[numeric] = {
        "code2": code2,
        "code3": code3,
        "name": country.name,
        "currency_code": currency_code,
        "currency_name": currency_name
    }


def getCountries():
    countryList = []
    for country in COUNTRIES:
        countryList.append((COUNTRIES[country]["code3"], COUNTRIES[country]["name"]))
    countryList.sort()

    return SortTuple(countryList)


def getCurrencies():
    currencyList = []
    for country in COUNTRIES:
        if COUNTRIES[country]["currency_code"]:
            currencyList.append((COUNTRIES[country]["currency_code"], COUNTRIES[country]["currency_name"]))
    # currencyList = [x for x in currencyList if x is not None]
    currencyList.sort()

    return SortTuple(currencyList)


def getMunicipalities(country_code):
    regionList = []
    regions = pycountry.subdivisions.get(country_code=country_code)
    for region in regions:
        regionList.append((region.code, region.name))
    return regionList


# Function to sort the list of tuples by its second item
def SortTuple(tup):

    # getting length of list of tuples
    lst = len(tup)
    for i in range(0, lst):

        for j in range(0, lst - i - 1):
            if (tup[j][1] > tup[j + 1][1]):
                temp = tup[j]
                tup[j] = tup[j + 1]
                tup[j + 1] = temp
    return tup

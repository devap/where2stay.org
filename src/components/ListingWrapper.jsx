import React, { useEffect } from 'react';
import _ from 'lodash';

import { getState } from './ContextState';
import ListingItems from './ListingItems';

const ListingsWrapper = () => {
  const [, setListings] = getState();
  const [{ filteredListings }, setFilteredListings] = getState();
  const [{ query }] = getState();

  useEffect(() => {
    const wsconn = new WebSocket('wss://where2stay.local/ws/');

    // to receive the message from server
    wsconn.onmessage = (message) => {
      const now = new Date();
      // const milliTime = now.getTime();
      const timestamp = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}:${now.getUTCMilliseconds()}`;
      console.log('wsconn.onmessage(', timestamp, ')');
      const newMessage = JSON.parse(message.data);
      const newListings = JSON.parse(newMessage.listings);
      console.log('wsconn.onmessage()->newListings: ', newListings);

      setListings({
        type: 'updateListings',
        listings: newListings,
      });

      setFilteredListings({
        type: 'updateFilteredListings',
        filteredListings: newListings,
      });
    };

    wsconn.onopen = () => {
      console.log('wsconn.onopen()->query: ', query);
      wsconn.send(JSON.stringify({
        type: 'websocket.listings_query',
        destination: query.destination,
        start_date: query.start_date,
        end_date: query.end_date,
        number_of_adults: query.number_of_adults,
        number_of_children: query.number_of_children,
        number_of_rooms: query.number_of_rooms,
      }));
    };

    wsconn.onerror = (error) => {
      console.log('wsconn.onerror()', error);
    };

    return () => {
      try {
        wsconn.close();
      } catch (e) {
        console.log('wsconn.close()->e: ', e);
      }
    };
  }, [query]);

  const ListingItemsArry = () => {
    console.log('63:ListingItemsArry().filteredListings: ', filteredListings);

    if (filteredListings && !_.isEmpty(filteredListings[0])) {
      return filteredListings.map(item => (
        <div className="listing" key={item.id}>
          <a href={`/listings/${item.id}`}>
            <div className="listing-photo">
              [photo]
            </div>
            {item.name}
          </a>
          <ListingItems
            pk={item.id}
            fields={item}
          />
        </div>
      ));
    }
    return <div>Loading...</div>;
  };

  const renderListings = () => (
    <div className="col-6 listings">
      <h1>Listings</h1>
      {ListingItemsArry()}
    </div>
  );

  return renderListings();
};

export default ListingsWrapper;

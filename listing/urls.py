# from django.contrib import admin
from django.urls import path

# from .admin import listing_admin_site

from .views import Listings, ListingSingle

app_name = 'listings'
urlpatterns = [
    # Listing Admin
    # path('admin/', admin.site.urls, name='listing_admin'),
    # path('admin/', listing_admin_site.urls, name='listing_admin'),
    path('<int:listing_id>/', ListingSingle.as_view(), name='listing'),
    path('', Listings.as_view(), name='listings'),
]

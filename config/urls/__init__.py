from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from djreservation import urls as djreservation_urls
# from _filebrowser import filebrowser_patterns
from ._rest import rest_patterns
from ._static import static_patterns
from content.urls import content_patterns

urlpatterns = [
    # Django Admin
    path('admin/', admin.site.urls),
    path('chaining/', include('smart_selects.urls')),

    # User authentication and management
    path('accounts/', include('allauth.urls')),
    path('accounts/', include('users.urls')),

    # Bookings
    path('listings/', include('listing.urls')),

    # Listings
    path('bookings/', include('booking.urls')),
    # path('reservation/', include('booking.urls')),

] + content_patterns

urlpatterns += rest_patterns
#urlpatterns += filebrowser_patterns
urlpatterns += static_patterns
urlpatterns += djreservation_urls.urlpatterns

if settings.DEBUG and settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

from django.views.generic.base import TemplateView
from djreservation.views import ProductReservationView

from .models import Booking


class Reservation(ProductReservationView):
    base_model = Booking
    amount_field = 'number_of_rooms'
    # extra_display_field = ['measurement_unit']  # not required


class Bookings(TemplateView):
    '''/listings
      ?param=value
      &aram=value
      &aram=value
      &aram=value
    '''
    template_name = 'booking/bookings.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        bookings = Booking.objects.all()
        context['booking'] = bookings
        return context

    def get(self, request, *args, **kwargs):
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)


class BookingSingle(TemplateView):
    '''/listings/<id>'''
    template_name = 'booking/bookings.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.kwargs:
            booking_id = self.kwargs.get('booking_id')

        bookings = Booking.objects.get(id=booking_id)
        context['bookings'] = bookings
        return context

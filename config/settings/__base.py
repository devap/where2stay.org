#from django.utils.translation import ugettext_lazy as _

#import sys
#reload(sys)
#sys.setdefaultencoding("utf8")

DEBUG = False
WEBPACK_DEV_SERVER = DEBUG

######### GENERAL CONFIGURATION
PROJET_NICKNAME = 'where2stay'
PROJET_DOMAIN_NAME = ''
TIME_ZONE = 'America/New_York'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
USE_THOUSAND_SEPARATOR = False
########## END GENERAL CONFIGURATION

# ######### SECURITY
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_BROWSER_XSS_FILTER = True
#SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_SECONDS = 3600
SESSION_COOKIE_AGE = 1209600  # 2 weeks
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_NAME = 'sessionid'
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_COOKIE_DOMAIN = None
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_DOMAIN = SESSION_COOKIE_DOMAIN
CSRF_COOKIE_SECURE = SESSION_COOKIE_SECURE
CSRF_COOKIE_AGE = 31449600  # approx 1 year
# ######### END SECURITY

CRISPY_TEMPLATE_PACK = 'bootstrap4'
TASTYPIE_ALLOW_MISSING_SLASH = True
APPEND_SLASH = True

########## MANAGER CONFIGURATION
# They get emails when things break, esp in production
# where debug is turned off
ADMINS = [
  ('Name', 'email@where2stay.org'),
]
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION

########## AUTHENTICATION BACKENDS
AUTH_USER_MODEL = 'users.User'
AUTHENTICATION_BACKENDS = (
  # Needed to login by username in Django admin, regardless of `allauth`
  'django.contrib.auth.backends.ModelBackend',

  # `allauth` specific authentication methods, such as login by e-mail
  'allauth.account.auth_backends.AuthenticationBackend',
)

# LOGIN/LOGOUT URLs
LOGIN_URL = '/accounts/login'
LOGIN_REDIRECT_URL = 'home'
LOGOUT_REDIRECT_URL = 'home'

# ALLAUTH
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
ACCOUNT_LOGIN_ATTEMPTS_LIMIT = 5
ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT = 86400  # 1 day
ACCOUNT_LOGOUT_REDIRECT_URL = LOGOUT_REDIRECT_URL  # defaults to '/accounts/login/'

ACCOUNT_FORMS = {
  'signup': 'users.forms.CustomSignupForm',
}

# SMART SELECTS
USE_DJANGO_JQUERY = False
JQUERY_URL = False

# Channels
ASGI_APPLICATION = "listing.routing.application"
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("localhost", 6379)],
        },
    },
}

# Google reCaptcha
RECAPTCHA_PUBLIC_KEY = '6Ldy6LkUAAAAAGtTzVtWj5JBZettW4UHBMMbBCHe'
########## END AUTHENTICATION BACKENDS

# ########## DATABASE CONFIGURATION
DATABASES = {
  'default': {
    #'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'NAME': '',
    'USER': '',
    'PASSWORD': '',
    'HOST': 'localhost',
    'PORT': 5432,
    'ATOMIC_REQUESTS': True
  }
}
# ########## END DATABASE CONFIGURATION

########## EMAIL CONFIGURATION
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_PORT = 1025
EMAIL_HOST = 'localhost'
DEFAULT_FROM_EMAIL = 'noreply@where2stay.org'
EMAIL_SUBJECT_PREFIX = '[where2stay]'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
########## END EMAIL CONFIGURATION

########## WSGI CONFIGURATION
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'config.wsgi.application'
########## END WSGI CONFIGURATION

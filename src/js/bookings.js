import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App_bookings';

require('../styl/bookings.styl');

const rootNode = document.getElementById('app_node_bookings');

ReactDOM.render(
  <App />,
  rootNode,
);

console.log('listings.js loaded');

# Generated by Django 2.2.6 on 2019-10-16 14:16

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import model_utils.fields
import smart_selects.db_fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.CITIES_CITY_MODEL),
        migrations.swappable_dependency(settings.CITIES_COUNTRY_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Listing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_listed', models.DateTimeField(auto_now_add=True, verbose_name='date listed')),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('number_of_rooms', models.IntegerField(verbose_name='number of Rooms')),
                ('distance_from_temple', models.DecimalField(decimal_places=2, max_digits=4, verbose_name='distance from temple')),
                ('distance_from_temple_units', models.CharField(blank=True, choices=[('K', 'kilometers'), ('M', 'miles')], max_length=1, verbose_name='units')),
                ('distance_from_temple_restaurant', models.DecimalField(decimal_places=2, max_digits=4, verbose_name='distance from temple restaurant')),
                ('distance_from_temple_restaurant_units', models.CharField(blank=True, choices=[('K', 'kilometers'), ('M', 'miles')], max_length=1, verbose_name='units')),
                ('distance_from_vegetarian_restaurant', models.DecimalField(decimal_places=2, max_digits=4, verbose_name='distance from vegetarian restaurant')),
                ('distance_from_vegetarian_restaurant_units', models.CharField(blank=True, choices=[('K', 'kilometers'), ('M', 'miles')], max_length=1, verbose_name='units')),
                ('cost', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='cost')),
                ('status', model_utils.fields.StatusField(choices=[('active', 'active'), ('offline', 'offline')], db_index=True, default='active', max_length=100, no_check_for_status=True)),
                ('city', smart_selects.db_fields.ChainedForeignKey(chained_field='state_province', chained_model_field='region_id', on_delete=django.db.models.deletion.CASCADE, related_name='cities', to=settings.CITIES_CITY_MODEL)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='countries', to=settings.CITIES_COUNTRY_MODEL, verbose_name='country')),
            ],
            options={
                'ordering': ('-distance_from_temple',),
                'get_latest_by': 'distance_from_temple',
            },
        ),
        migrations.CreateModel(
            name='Amenities',
            fields=[
                ('listing', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='amenities', serialize=False, to='listing.Listing')),
                ('shared_room', models.BooleanField(verbose_name='shared room')),
                ('private_room', models.BooleanField(verbose_name='private room')),
                ('shared_bathroom', models.BooleanField(verbose_name='shared bathroom')),
                ('private_bathroom', models.BooleanField(verbose_name='private bathroom')),
                ('whole_apartment', models.BooleanField(verbose_name='whole apartment')),
                ('prasadam_included', models.BooleanField(verbose_name='prasadam included')),
                ('car_for_hire_available', models.BooleanField(verbose_name='car for hire available')),
                ('airport_pickup_available', models.BooleanField(verbose_name='airport pickup available')),
                ('children_ok', models.BooleanField(default=True, verbose_name='children ok?')),
            ],
        ),
    ]

import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

import { addDays } from '../js/utils';

const DatePickerField = ({
  values,
  setFieldValue,
}) => (
  <>
    <DatePicker
      selectsStart
      className="query-panel start-date"
      startDate={values.startDate || 'Date From'}
      endDate={values.endDate}
      selected={values.startDate}
      minDate={addDays(new Date(), 1)}
      onChange={(v) => {
        setFieldValue('startDate', v);
        const mDate = moment(v).format('YYYY/MM/DD');
        const strDate = mDate.toString();
        console.log('strDate: ', strDate);
        setFieldValue('startDateStr', v);
      }}
      value={values.startDate || 'Date From'}
    />

    <div className="query-panel date-range-divider oi oi-arrow-right" />

    <DatePicker
      selectsEnd
      className="query-panel end-date"
      endDate={values.endDate || 'Date To'}
      selected={values.endDate}
      minDate={values.startDate}
      onChange={(v) => {
        setFieldValue('endDate', v);
        const mDate = moment(v).format('YYYY/MM/DD');
        const strDate = mDate.toString();
        console.log('strDate: ', strDate);
        setFieldValue('endDateStr', v);
      }}
      value={values.endDate || 'Date To'}
    />
  </>
);

DatePickerField.propTypes = {
  values: PropTypes.shape({
    startDate: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string
    ]),
    endDate: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string
    ]),
  }),
  setFieldValue: PropTypes.func.isRequired,
};

DatePickerField.defaultProps = {
  values: {
    startDate: null,
    endDate: null,
  },
};

export default DatePickerField;

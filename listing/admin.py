from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .models import Amenities, Listing

admin.site.login = login_required(admin.site.login)

'''
class ListingAdminSite(admin.AdminSite):
    listing_site_header = 'Where2Stay Listing Admin'
    site_title = 'Where2Stay Listing Admin Portal'
    index_title = 'Welcome to Where2Stay Listing Admin Portal'


listing_admin_site = ListingAdminSite(name='listings')
listing_admin_site.login = login_required(admin.site.login)

# listing_admin_site.register(Listing, ListingAdmin)
# admin.site.register(Listing, ListingAdmin)

@admin.register(Listing, site=listing_admin_site)

'''


class AmenitiesInline(admin.TabularInline):
    model = Amenities
    can_delete = False


@admin.register(Listing)
class ListingAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_listed'
    inlines = [AmenitiesInline]
    list_display = ['id',
                    'status',
                    'name',
                    'host',
                    'city',
                    'country',
                    'number_of_rooms',
                    'get_cost',
                    'currency',
                    ]

    list_display_links = ['id', 'name', ]
    list_filter = ['city', 'country', ]
    list_select_related = True
    ordering = ['-id', ]
    radio_fields = {'status': admin.HORIZONTAL}
    readonly_fields = ['date_listed', 'get_cost', 'currency', ]
    save_as = True
    save_on_top = True
    search_fields = ['id',
                     'name',
                     'city',
                     'country',
                     ]

    show_full_result_count = True

    fieldsets = (
      ("Content",
        {"fields":
          (
           ("name", "status"),
           "country",
           ("state_province", "city", ),
           )
         }
      ),
      ("Metadata",
        {"fields":
          (
           "number_of_rooms",
           ("cost", "currency", ),

           ("distance_from_temple", "distance_from_temple_units"),
           ("distance_from_temple_restaurant", "distance_from_temple_restaurant_units"),
           ("distance_from_vegetarian_restaurant", "distance_from_vegetarian_restaurant_units"),
           )
        }
      ),
      ("Info",
        {"fields":
          ("date_listed", ),
         "classes": ('grp-collapse grp-closed',)
        }
      ),
     )

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.host = request.user
        super().save_model(request, obj, form, change)

    class Media:
        js = ('dist/admin.js',)
        css = {
            'all': ('dist/admin.css',)
        }

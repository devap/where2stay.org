upstream wsocket {
	server unix:/opt/code/where2stay.org/uvicorn.sock;
}

map $http_upgrade $connection_upgrade {
     default upgrade;
     '' close;
}

server {
  listen 80 http2;

  server_name
    where2stay.local
  ;

  return 301 https://$host$request_uri;
}

server {
  # listen 80 http2;
  listen 443 http2 ssl;

  server_name
    where2stay.local
  ;

  if ($http_host !~ "^where2stay.local$") {
    rewrite ^/(.*)$ https://where2stay.local/$1 permanent;
  }

  ssl_certificate /opt/code/where2stay.org/config/nginx/certs/where2stay.ssl.crt;
  ssl_certificate_key /opt/code/where2stay.org/config/nginx/certs/where2stay.ssl.key;

  root /opt/code/where2stay.org;

  location /files/ {
      root /Users/devaprastha/data/where2stay/media/;
      expires max;
  }

  location /media/ {
      root /Users/devaprastha/data/where2stay/;
      expires max;
  }

  location ~ ^/static/ {
      root /Users/devaprastha/data/where2stay/;
      expires max;
  }

  location /imgvers/ {
      root /Users/devaprastha/data/where2stay/media;
      expires max;
  }

  location /img/ {
      root /Users/devaprastha/data/where2stay/static/;
      expires max;
  }

  location /images/ {
      root /Users/devaprastha/data/where2stay/media;
      expires max;
  }

  location /robots.txt {
    root /Users/devaprastha/data/where2stay/static/;
  }

  location /favicon.ico {
    alias /Users/devaprastha/data/where2stay/static/img/favicon.ico;
    log_not_found off;
    access_log off;
  }

  location ~ /apple-touch-icon.* {
      root /Users/devaprastha/data/where2stay/static/img/touch;
      access_log on;
      log_not_found on;
  }

  location ~ ^/.+\.(css|js) {
      root /Users/devaprastha/data/where2stay/static/dist/;
      expires max;
  }

  # this is the endpoint of the channels routing
  location /ws/ {
      # proxy_pass http://wsocket;
      proxy_pass https://127.0.0.1:8000;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_buffering off;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_redirect off;
  }

  # Finally, send all non-media requests to the Django server.
  location / {
    uwsgi_buffering off;
    include uwsgi_params;

    uwsgi_pass_request_headers on;
    uwsgi_no_cache $cookie_nocache  $arg_nocache$arg_comment;
    uwsgi_no_cache $http_pragma     $http_authorization;
    uwsgi_cache_bypass $cookie_nocache $arg_nocache $arg_comment;
    uwsgi_cache_bypass $http_pragma $http_authorization;

    uwsgi_pass  unix:/opt/code/where2stay.org/uwsgi.sock;

  }

    # what to serve if upstream is not available or crashes
    error_page 404             /404.html;
    error_page 500 502 503 504 /500.html;

  access_log /opt/code/where2stay.org/logs/nginx-access.log;
  error_log /opt/code/where2stay.org/logs/nginx-error.log;

}

from django.views.generic.base import TemplateView

from rest_framework.viewsets import ModelViewSet

from .models import Listing
# from users.models import User
from .serializers import ListingSerializer
# from .notifications import send_listings


# ViewSets define the view behavior.
class ListingViewSet(ModelViewSet):
    queryset = Listing.active.all()
    serializer_class = ListingSerializer


class Listings(TemplateView):
    '''/listings
      ?destination=Mayapura
      &bookAdults=1
      &bookChildren=0
      &bookRooms=1
    '''
    template_name = 'listing/listings.html'

    destination = ''
    startDateStr = None
    endDateStr = None
    bookAdults = None
    bookChildren = None
    bookRooms = None

    def dispatch(self, request, *args, **kwargs):
        '''
        request.session.create()
        request.session['session_key'] = request.session.session_key
        self.session = request.session
        Client.objects.create(session=self.session["session_key"])
        '''
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        # print('Listings().post()->self.session["session_key"]: ', self.session['session_key'])

        self.destination = request.POST.get('destination')
        self.startDateStr = request.POST.get('startDateStr')
        self.endDateStr = request.POST.get('endDateStr')
        self.bookAdults = request.POST.get('bookAdults')
        self.bookChildren = request.POST.get('bookChildren')
        self.bookRooms = request.POST.get('bookRooms')

        context = self.get_context_data(**kwargs)
        # session_key = self.session['session_key']

        # listings = Listing.active.all()
        # listings = Listing.active.filter(city__name_std=self.destination)

        '''
            .filter(startDate=startDateStr) \
            .filter(endDate=endDateStr) \
            .filter(number_of_rooms=bookRooms) \
            .filter(children_ok=bookChildren)
        '''
        # print('views.post()->listings: ', listings)
        # send_listings(query, listings, session_key)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # context = {}
        # context['listings'] = listings
        context['destination'] = self.destination
        context['startDateStr'] = self.startDateStr
        context['endDateStr'] = self.endDateStr
        context['bookAdults'] = self.bookAdults
        context['bookChildren'] = self.bookChildren
        context['bookRooms'] = self.bookRooms

        return context


class ListingSingle(TemplateView):
    '''/listings/<id>'''
    template_name = 'listing/listing_single.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.kwargs:
            listing_id = self.kwargs.get('listing_id')

        listing = Listing.objects.get(id=listing_id)
        context['listing'] = listing
        return context

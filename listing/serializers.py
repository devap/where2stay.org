from rest_framework.serializers import ModelSerializer

from cities.models import City, Country, Region
from .models import Amenities, Listing


class CitySerializer(ModelSerializer):
    class Meta:
        model = City
        # fields = '__all__'
        fields = ['name', ]


class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        # fields = '__all__'
        fields = [
            'name',
            'currency',
            'currency_symbol',
        ]


class RegionSerializer(ModelSerializer):
    class Meta:
        model = Region
        # fields = '__all__'
        fields = ['name', ]


class AmenitiesSerializer(ModelSerializer):
    class Meta:
        model = Amenities
        fields = '__all__'


class ListingSerializer(ModelSerializer):
    amenities = AmenitiesSerializer(read_only=True)
    city = CitySerializer(read_only=True)
    country = CountrySerializer(read_only=True)
    state_province = RegionSerializer(read_only=True)

    class Meta:
        model = Listing
        # fields = '__all__'
        fields = [
            'id',
            'date_listed',
            'name',
            'number_of_rooms',
            'distance_from_temple',
            'distance_from_temple_units',
            'distance_from_temple_restaurant',
            'distance_from_temple_restaurant_units',
            'distance_from_vegetarian_restaurant',
            'distance_from_vegetarian_restaurant_units',
            'cost',
            'status',
            'host',
            'city',
            'country',
            'state_province',
            'amenities',
        ]

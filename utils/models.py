from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    name = models.CharField(
        _('name'),
        max_length=155,
     )

    numeric = models.IntegerField(
        _('country code'),
        primary_key=True,
     )

    code2 = models.CharField(
        _('abbreviation 2-Ltr'),
        max_length=2
     )

    code3 = models.CharField(
        _('abbreviation 3-Ltr'),
        max_length=3
     )

    currency_code = models.CharField(
        _('currency code'),
        max_length=4,
        blank=True,
     )

    currency_name = models.CharField(
        _('currency name'),
        max_length=55,
        blank=True,
     )

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "Countries"

    def __str__(self):
        return self.name


class Region(models.Model):

    name = models.CharField(
        _('state, province or municipality'),
        max_length=155,
     )

    code = models.CharField(
        _('code'),
        max_length=6
     )

    country = models.IntegerField(
        _('country ID'),
     )

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "Codes"

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(
        _('state, province or municipality'),
        max_length=155,
     )

    code = models.CharField(
        _('code'),
        max_length=6
     )

    country = models.IntegerField(
        _('country ID'),
     )

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "Codes"

    def __str__(self):
        return self.name

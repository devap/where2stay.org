from django.urls import path, re_path

from content.views import (
  Homepage,
  PageDetail,
  Thanks
)

app_name = 'content'
content_patterns = [

   path('', Homepage.as_view(), name='home'),

   path('page/<int:page_id>/',
        PageDetail.as_view(),
        name='page'
        ),

   # /thanks
   path('thanks/',
        Thanks.as_view(), {'type': 'general'},
        name='thanks_donate'
        ),

]

import React from 'react';
import PropTypes from 'prop-types';

/*
const childCnt = 11;
const options = [];
for (let i = 0; i < childCnt; i += 1) {
  const option = {
    value: i,
    label: (i === 0 || i > 1) ? `${i} Children` : `${i} Child`,
  };
  options.push(option);
}
*/

const Children = ({
  value,
  changeHandler,
}) => (
  <select
    name="bookChildren"
    value={value}
    onChange={changeHandler}
  >
    <option value="0">0 Children</option>
    <option value="1">1 Child</option>
    <option value="2">2 Children</option>
    <option value="3">3 Children</option>
    <option value="4">4 Children</option>
    <option value="5">5 Children</option>
    <option value="6">6 or more Children</option>
  </select>
);

Children.propTypes = {
  value: PropTypes.string.isRequired,
  changeHandler: PropTypes.func.isRequired,
};

export default Children;

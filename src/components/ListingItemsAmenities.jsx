import React from 'react';
import {
  FaCheck,
  FaTimes,
  FaGopuram,
  FaUtensils
} from 'react-icons/fa';

import { IconContext } from 'react-icons';

const Amenities = (props) => {
  const {
    distance_from_temple,
    distance_from_temple_units,
    distance_from_temple_restaurant,
    distance_from_temple_restaurant_units,
    distance_from_vegetarian_restaurant,
    distance_from_vegetarian_restaurant_units
  } = props.attribs;

  const {
    shared_room,
    private_room,
    whole_apartment,
    shared_bathroom,
    private_bathroom,
    prasadam_included,
    car_for_hire_available,
    airport_pickup_available,
  } = props.amenities;

  const icon = (field) => {
    if (field) {
      return <FaCheck className="facheck" />;
    }
    return <FaTimes className="fatimes" />;
  };

  const distance = (KM) => {
    if (KM === 'K') {
      return ' Kilometers';
    }
    return ' Miles';
  };

  return (
    <IconContext.Provider value={{ className: 'react-icons' }}>
      <li className="amenities-item">
        {icon(shared_room)}
        Shared Room
      </li>

      <li className="amenities-item">
        {icon(private_room)}
        Private Room
      </li>

      <li className="amenities-item">
        {icon(whole_apartment)}
        Whole Apartment
      </li>

      <li className="amenities-item">
        {icon(shared_bathroom)}
        Shared Bathroom
      </li>

      <li className="amenities-item">
        {icon(private_bathroom)}
        Private Bathroom
      </li>

      <li className="amenities-item">
        {icon(prasadam_included)}
        Prasadam Included
      </li>

      <li className="amenities-item">
        {icon(car_for_hire_available)}
        Car for Hire Available
      </li>

      <li className="amenities-item">
        {icon(airport_pickup_available)}
        Airport Pickup Available
      </li>

      <li className="amenities-item">
        <FaGopuram className="fagopuram" />
        Distance from Temple:&nbsp;
        {distance_from_temple.replace(/^0+(\d)|(\d)0+$/gm, '$1$2')}
        {distance(distance_from_temple_units)}
      </li>

      <li className="amenities-item">
        <FaUtensils className="fautensils" />
        Distance from Temple Restaurant:&nbsp;
        {distance_from_temple_restaurant.replace(/^0+(\d)|(\d)0+$/gm, '$1$2')}
        {distance(distance_from_temple_restaurant_units)}
      </li>

      <li className="amenities-item">
        <FaUtensils className="fautensils" />
        Distance from Vegetarian Restaurant:&nbsp;
        {distance_from_vegetarian_restaurant.replace(/^0+(\d)|(\d)0+$/gm, '$1$2')}
        {distance(distance_from_vegetarian_restaurant_units)}
      </li>
    </IconContext.Provider>
  );
};

export default Amenities;

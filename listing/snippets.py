
channel_layer = get_channel_layer()
channel_name = Client.objects.get(session=self.session.session_key).channel
print('msg_listings()->channel_name: ', channel_name)
channel_layer.send(channel_name, {
    "msg_type": settings.MSG_TYPE_LISTINGS,
    "text": listings,
})

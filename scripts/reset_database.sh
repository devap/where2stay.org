#!/usr/bin/env bash

#copy database from admin server
rm -f ./data/where2stay.prod.sql.gz
rm -f ./data/where2stay.prod.sql
scp -C inews:/var/www/where2stay.org/data/where2stay.prod.sql.gz ./data/
gunzip ./data/where2stay.prod.sql.gz

# reset the database
echo "drop database where2stay"
dropdb where2stay

echo "create database where2stay"
createdb -O where2stay -T template0 where2stay

echo "load data ..."
psql where2stay < ./data/where2stay.prod.sql

echo "Congratulations -- we're All Done!"

echo "Remember to chant Hare Krishna and your life will be sublime - yes, really :^)!"


import * as Yup from 'yup';
import moment from 'moment';

const Validations = () => (
  Yup.object().shape({
    destination: Yup.string()
      .required('Destination is required'),

    startDate: Yup.date()
      .min(moment().toDate())
      .when('endDate', {
        is: v => v != null,
        then: s => s.max(Yup.ref('endDate'))
      })
      .required('Start Date is required'),

    endDate: Yup.date()
      .min(Yup.ref('startDate'))
      .required('End Date is required'),

  })

);

/*
await Validations.validate({
    startDate: moment().toDate(),
    endDate: moment().add(-1, 'day').toDate()
})
*/

export default Validations;

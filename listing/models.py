from django import forms
from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.fields import StatusField

from smart_selects.db_fields import ChainedForeignKey
from cities.models import Country, Region, City

# from users.models import User


"""
Code Styling:

    All database fields
    Custom manager attributes
    class Meta
    def __str__()
    def save()
    def get_absolute_url()
    Any custom methods
"""


class ListingManager(models.Manager):
    """Custom manager"""

    def get_queryset(self):
        return super(ListingManager, self).get_queryset().filter(status='active')


class Amenities(models.Model):

    listing = models.OneToOneField(
        'Listing',
        primary_key=True,
        on_delete=models.CASCADE,
        related_name='amenities',
     )

    shared_room = models.BooleanField(_('shared room'))
    private_room = models.BooleanField(_('private room'))
    shared_bathroom = models.BooleanField(_('shared bathroom'))
    private_bathroom = models.BooleanField(_('private bathroom'))
    whole_apartment = models.BooleanField(_('whole apartment'))
    prasadam_included = models.BooleanField(_('prasadam included'))
    car_for_hire_available = models.BooleanField(_('car for hire available'))
    airport_pickup_available = models.BooleanField(_('airport pickup available'))

    children_ok = models.BooleanField(
        _('children ok?'),
        default=True,
     )


class Listing(models.Model):

    date_listed = models.DateTimeField(_('date listed'), auto_now_add=True)

    distance_units = Choices(
        ('K', 'Kilometers', _('kilometers')),
        ('M', 'Miles', _('miles'))
     )

    name = models.CharField(
        _('name'),
        max_length=255,
     )

    host = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='hosts',
        verbose_name=_('host'),
     )

    country = models.ForeignKey(
        Country,
        on_delete=models.PROTECT,
        related_name='countries',
        verbose_name=_('country'),
     )

    state_province = ChainedForeignKey(
        Region,
        chained_field='country',
        chained_model_field="country_id",
        show_all=False,
        auto_choose=False,
        related_name='states',
        sort=True,
     )

    city = ChainedForeignKey(
        City,
        chained_field='state_province',
        chained_model_field="region_id",
        show_all=False,
        auto_choose=False,
        related_name='cities',
        sort=True,
     )

    number_of_rooms = models.IntegerField(
        _('number of Rooms'),
     )

    distance_from_temple = models.DecimalField(
        _('distance from temple'),
        decimal_places=2,
        max_digits=4,
     )

    distance_from_temple_units = models.CharField(
        _('units'),
        choices=distance_units,
        max_length=1,
        blank=True,
     )

    distance_from_temple_restaurant = models.DecimalField(
        _('distance from temple restaurant'),
        decimal_places=2,
        max_digits=4,
     )

    distance_from_temple_restaurant_units = models.CharField(
        _('units'),
        choices=distance_units,
        max_length=1,
        blank=True,
     )

    distance_from_vegetarian_restaurant = models.DecimalField(
        _('distance from vegetarian restaurant'),
        decimal_places=2,
        max_digits=4,
     )

    distance_from_vegetarian_restaurant_units = models.CharField(
        _('units'),
        choices=distance_units,
        max_length=1,
        blank=True,
     )

    cost = models.DecimalField(
        _('cost'),
        decimal_places=2,
        max_digits=7,
     )

    STATUS = Choices(
        'active',
        'offline'
    )
    status = StatusField(
        db_index=True,
     )

    # The default manager.
    objects = models.Manager()
    # custom Manager
    active = ListingManager()

    class Meta:
        ordering = ("-distance_from_temple",)
        get_latest_by = ("distance_from_temple")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('listings:listing', kwargs={'listing_id': self.id})

    def get_cost(self):
        symbol = Country.objects.get(id=self.country_id).currency_symbol
        return '%s %.2f' % (symbol, self.cost)
    get_cost.short_description = 'Cost'
    get_cost.admin_order_field = '-cost'

    def get_currency(self):
        return '%s %s' % (self.get_cost, self.currency)

    @property
    def currency(self):
        return Country.objects.get(id=self.country_id).currency

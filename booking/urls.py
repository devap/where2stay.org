from django.urls import path

# from .admin import booking_admin_site
from .views import Bookings, BookingSingle

app_name = 'bookings'
urlpatterns = [
    # Booking Admin
    # path('admin/', booking_admin_site.urls),
    path('create/<int:booking_id>', Bookings.as_view(), 'reservation'),
    path('<int:booking_id>', BookingSingle.as_view(), name='booking'),
    path('', Bookings.as_view(), name='bookings'),
]
